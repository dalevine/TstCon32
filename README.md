I updated the project to compile with VS2015. I have only built and tested the Win32 Debug configuration. The weird thing about the settings is that all the configurations, including the 64 bit config, is that they all build to the same executable file, TstCon32.exe. 

If you need support for different platforms you should modify the configuration to build to unique filenames or directories.



